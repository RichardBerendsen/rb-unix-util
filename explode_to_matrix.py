"""
explode_to_matrix.py

GPL3, Nov 2012
Richard Berendsen

Usage:
    explode_to_matrix.py f_rows f_cols f_vals

Where f_rows is an integer specifying the field to use row labels from,
f_cols is an integer specifying the field to use column labels from,
and f_vals is an integer specifying the field to use value labels from.

Output:

    a matrix with a header consisting of n unique column labels found
    (in order of appearance), followed by m lines which consist of a row
    label (in order of appearance) followed by n values.

    Duplicate values for unique row-column combinations are removed, the
    last seen value is kept.

    It is possible to use the same column for all three arguments, or
    any two arguments, should this ever come in handy.

Options:

    -h Show this help
    -d Delimiter (separator) of fields
"""

import sys
import re
import os
import getopt
import collections

def main(argv):

    try:
        opts, args = getopt.getopt(argv[1:], "hd:")
    except getopt.GetoptError, err:
        print >> sys.stderr, str(err)
        print >> sys.stderr, __doc__
        return 2

    # catch conflicting options

    # defaults
    sep = r"\s+"
    sep_out = "\t"
    list = ""
    for o, a in opts:
        if o == "-h":
            print __doc__
            return 0
        elif o == "-d":
            sep = a
            sep_out = a

    if len(args) != 3:
        print >> sys.stderr, "%s: Wrong number of arguments."
        print >> sys.stderr, __doc__
        return 2

    col_rows = int(args[0]) - 1
    col_cols = int(args[1]) - 1
    col_vals = int(args[2]) - 1

    rows = collections.OrderedDict()
    cols = collections.OrderedDict()
    A = collections.defaultdict(lambda: "NA")

    # read in matrix from stdin
    for l in sys.stdin:
        fields = re.split(sep, l.rstrip('\n'))
        row = fields[col_rows]
        col = fields[col_cols]
        val = fields[col_vals]
        rows[row] = None
        cols[col] = None
        A[(row,col)] = val

    # and write it out
    print sep_out.join(cols.keys())
    for row in rows.keys():
        print sep_out.join([row] + [A[(row,col)] for col in \
            cols.keys()])

    # should be as simple as that.
    return 0

if __name__ == "__main__":
    sys.exit(main(sys.argv))
