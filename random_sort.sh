#!/bin/bash

usage() {
cat >&2 <<EOF
random_sort.sh
    file

Options:
    -b buffer_size (used in sort to speed things up)
    -t temporary_directory (used in sort, and also used to store intermediate
            sorted files)
    -s seed (used to initialize RANDOM)
    -o out (out file, otherwise print tos stdout)

Explanation:
random_sort.sh performs a random sort of 'file'. But first, it
sorts the two files lexcigraphically on their respective join fields, always
a pain to do manually. Its output is sorted lexicographically on the join field as
well, as a consequence. In addition, to speed up the sort, this script accepts
parameters such as buffer_size and temporary_directory for unix sort.

Note:
There is also on many systems the command sort -R, but it is not available in
all bash versions.
EOF
}

# default values:
temporary_directory='/tmp'
buffer_size='10M'
out='' # stdout
seed=$RANDOM

# get options
while getopts "b:t:o:s:" opt; do
  case "$opt" in
    b)
      buffer_size="$OPTARG"
      ;;
    t)
      temporary_directory="$OPTARG"
      ;;
    o)
      out="$OPTARG"
      ;;
    s)
      seed="$OPTARG"
      ;;
    *)
      echo "Invalid option: -$OPTARG" >&2
      ;;
  esac
done
shift $(($OPTIND - 1))

if [ "$#" -ne '1' ]
then
    usage
    exit 2
fi

f="$1"
[ -r "$f" ] || exit 66 # find /usr -name sysexits.h, can't read input

# seed RANDOM
RANDOM="$seed"

# make temporary directory in temporary_directory for output sort
my_tmp="$( mktemp -d "$temporary_directory/random_sort.XXXXXXXXXX" )" \
        || exit "$?"

# if -o is specified, write output there instead of to stdout.
# Note: we just overwrite it if it exists, or attempt to, anyway.
if [ "$out" != '' ]
then
    exec > "$out" || exit "$?"
            # exec without command simply changes redirection of current
            # script, see man exec
            # Note, we keep stderr as normal to stderr.
fi

# add random integer to each line
# Note: possibly this can be made faster using awk.
while read line 
do
    echo -e "$RANDOM\t$line" >> "$my_tmp/in.with_random_int" || exit "$?"
done < "$f"

# Then sort by this RANDOM integer and output everything but that integer.
LC_ALL=C sort -k1,1n \
        --temporary-directory="$temporary_directory" \
        --buffer-size="$buffer_size" \
        "$my_tmp/in.with_random_int" \
| cut --complement --fields=1 || exit "$?"
# Note, if sort fails, but cut succeeds, we will not catch the error.
# We could use ${PIPESTATUS[@]} for that, but it is bash specific and I'll
# leave it for later.

# clean up after yourself
rm -rf "$my_tmp" || exit "$?"
