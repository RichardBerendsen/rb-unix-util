# Change the mode of operation of bash scripts (error reporting and exiting)
# NB: I don't really like this anymore, since not all errors are consistently
# caught by bash. If you want it done correctly, you will have to check exit
# statuses of every command you do yourself.
set -o errtrace # make subshells and so on inherit trap
set -o nounset
signoff() {
    echo "$0: Error, line $1, exit status $2, exiting" >&2
    exit $2
}
trap 'signoff ${LINENO} $?' ERR

# These were taken from my /usr/include/sysexits.h
readonly EX_USAGE=64			# command line usage error */
readonly EX_DATAERR=65		# data format error */
readonly EX_NOINPUT=66		# cannot open input */
readonly EX_NOUSER=67		# addressee unknown */
readonly EX_NOHOST=68		# host name unknown */
readonly EX_UNAVAILABLE=69	# service unavailable */
readonly EX_SOFTWARE=70		# internal software error */
readonly EX_OSERR=71			# system error (e.g., can't fork) */
readonly EX_OSFILE=72		# critical OS file missing */
readonly EX_CANTCREAT=73		# can't create (user) output file */
readonly EX_IOERR=74			# input/output error */
readonly EX_TEMPFAIL=75		# temp failure; user is invited to retry */
readonly EX_PROTOCOL=76		# remote error in protocol */
readonly EX_NOPERM=77		# permission denied */
readonly EX_CONFIG=78		# configuration error */

# Some functions that may come in handy in any bash script.
# General error handling philosophy of these functions: After human error in
# calling, echo informative message and return 2.  After more unlikely failures
# such as I/O failures, just return exit status of command. These commands
# will have printed error messages themselves, normally.

redirect_stdout() {
    if [ "$#" -ne '1' ]
    then 
        echo 'redirect_stdout: wrong number of arguments' >&2
        return 2
    fi
    local f_out="$1"
    if [ -e "$f_out" ]
    then
        echo 'redirect_stdout: file exists' >&2
        return 2
    fi
    local d # declare first to capture exit status dirname
    d="$( dirname "$f_out" )" || return "$?"
    mkdir --parents "$d" || return "$?"
    exec > "$f_out" || return "$?"
    return 0
}

redirect_stderr() {
    if [ "$#" -ne '1' ]
    then 
        echo 'redirect_stderr: wrong number of arguments' >&2
        return 2
    fi
    local f_out="$1"
    if [ -e "$f_out" ]
    then
        echo 'redirect_stderr: file exists' >&2
        return 2
    fi
    local d # declare first to capture exit status dirname
    d="$( dirname "$f_out" )" || return "$?"
    mkdir --parents "$d" || return "$?"
    exec 2> "$f_out" || return "$?"
    return 0
}

check_pipe() {
    local PS=( "${PIPESTATUS[@]}" ) # Copy PIPESTATUS before any other command
    [ "$#" -eq '2' ] || return "$EX_USAGE"
    local scriptname="$1"
    local lineno="$2"
    local i
    for i in "${!PS[@]}"
    do
        if local s="${PS[$i]}" && [ "$s" -ne 0 ]
        then
            echo "$scriptname, line $lineno: command $i in pipe exited with status $s" >&2
            return "$s"
        fi
    done
}

right_number_of_arguments_or_die() {
    if [ $# -ne 2 ]
    then
        echo "$0: Wrong number of arguments" >&2
        exit 2
    fi
    if [ $1 -ne $2 ]
    then
        right_number_of_arguments_or_die
    fi
}
# Note that the functions below check the exit status
# of functions they use. This is because the above error
# trapping functions only catch a non zero exit code at
# the main script level.
# NB: == realpath? No, realpath is not that widely available.
# NB2: readlink -f would do always.
get_abs_path() {
    right_number_of_arguments_or_die $# 1 || exit 2
    local PARENT_DIR=$(dirname "$1")
    cd "$PARENT_DIR"
    local ABS_PATH="$(pwd)"/"$(basename $1)"
    cd - >/dev/null
    echo $ABS_PATH
}
exists_or_die() {
    right_number_of_arguments_or_die $# 1 || exit 2
    if [ ! -e "$1" ]
    then
        echo "$0: $1: No such file or directory" >&2
        exit 2
    fi
}
is_dir_or_die() {
    right_number_of_arguments_or_die $# 1 || exit 2
    if [ ! -d "$1" ]
    then
        echo "$0: $1: No such directory" >&2
        exit 2
    fi
}
is_empty_dir_or_die() {
    right_number_of_arguments_or_die $# 1 || exit 2
    is_dir_or_die "$1" || exit 2
    if [ ! -z "$(ls -A "$1")" ]
    then
        echo "$0: $1: Directory not empty" >&2
        exit 2
    fi
}
does_not_exist_or_die() {
    right_number_of_arguments_or_die "$#" 1 || exit 2
    if [ -e "$1" ]
    then
        echo "$0: $1: File exists" >&2
        exit 2
    fi
}
### The next gem checks if a src repo is clean
# That is,
# --they have no files which are both untracked and unignored
# --tracked files are in the same state as HEAD
# (Note that the cache could differ from the HEAD if there are unstaged changes
# that undo the cached changes)
# See: http://stackoverflow.com/a/2659808/817777
# for a very nice exposition of how to do this check properly.
git_repo_clean_or_die() {
    is_dir_or_die $1 || exit 1
    # put the whole check in a subprocess to avoid cd'ing caller
    (
    cd $1
    # exit on unexpected ls-files error
    gls=$( git ls-files --others --exclude-standard )
    if [ "$?" -ne "0" ]
    then
        echo "Unexpected error from git ls-files" >&2
        exit 1
    fi
    # exit if $gls is not null (there are files which are untracked and
    # unignored)
    if [ -n "$gls" ]
    then
        echo -e "Untracked (and unignored)\
files present in git repo:\n$1" >&2
        exit 1
    fi
    # exit if git diff-index has a non-zero exit status
    git diff-index --quiet HEAD
    if [ "$?" -ne "0" ]
    then
        echo -e "Working directory differs from HEAD in:\n$1" >&2
        exit 1
    fi
    ) || exit 1
}

# get_key key_name file
#   assumes two column tab separated format in file; (actually, just splits on
#   first white space) reads file line by line, as soon as key_name is
#   encountered in the first column, the second column is returned. If key_name
#   is not encountered, barks.
get_key() {
    right_number_of_arguments_or_die "$#" 2 || exit 2
    exists_or_die "$2" || exit 2
    while read key value
    do
        if [ "$key" == "$1" ]
        then
            echo -n "$value"
            return 0
        fi
    done < "$2"
    echo "get_key: key '$1' not found" >&2
    return 3 # key not found
}

get_key_or_default() {
    right_number_of_arguments_or_die "$#" 3 || exit 2
    exists_or_die "$3" || exit 2
    if get_key "$1" "$3" 2>/dev/null
    then
        # echo has already taken place in the if statement
        return 0
    else
        echo -n "$2"
        return 0
    fi
}

# set_key key_name value file
# Will add or overwrite key-value pair.
# If file does not exist, it will be created.
set_key() {
    right_number_of_arguments_or_die "$#" 3 || exit 2
    new_key="$1"
    new_value="$2"
    file="$3"
    if [ -e "$file" ]
    then
        # remove key if present
        copy="$( mktemp /tmp/gaston.key_value.XXXXXXXXXX )"
        while read key value
        do
            if [ "$key" != "$new_key" ]
            then
                echo -e "$key\t$value" >> "$copy"
            fi
        done < "$file"
        mv "$copy" "$file"
    else
        # create file 
        d="$( dirname "$file" )"
        b="$( basename "$file" )"
        mkdir --parents "$d"
        touch "$d/$b"
    fi
    echo -e "$new_key\t$new_value" >> "$file"
    return 0
}

# find_one dir path 
# use find_one in cases where you know you should find one and only one file
# matching a certain pattern.
find_one() {
    right_number_of_arguments_or_die "$#" 2 || exit 2
    is_dir_or_die "$1" || exit 2

    files=( $( find "$1" -path "$2" ) )
    if [ "${#files[@]}" -ne "1" ]
    then
        echo "find_one: not exactly one file found" >&2
        echo "find_one: files:" >&2
        for f in "${files[@]}"; do echo "$f"; done
        exit 2
    fi
    echo "${files[0]}"
    return 0
}

# list membership, uses some Bash-only tricks
is_in() {
   local e 
   for e in "${@:2}"
   do
       [[ "$e" == "$1" ]] && return 0
   done
   return 1
}

# for logging:
readonly me="$( basename "$0" )"
readonly timec="$( which time )"
readonly timef='%C (%x)\nelapsed: %E, usr: %U, sys: %S'
elapsed() {
    right_number_of_arguments_or_die "$#" 1 || exit 2
    local readonly now=$( date +"%s" )
    local readonly diff=$(( $now - $1 ))
    date -u -d @"$diff" +'%-Hh %-Mm %-Ss'
}
readonly startdate=$( date +"%s" )
readonly startdatestr="$( date )"
