"""
uniq.py

GPL3, Nov 2012
Richard Berendsen

uniq.py file

Options:

    -h Show this help
    -d Delimiter (separator) of fields
    -f LIST
       Compare lines only on the basis of these fields

    LIST is a comma separated list of ranges. A range is either a single
    integer, or a start and endpoint (both inclusive) separated by a hyphen.
    If start- or endpoint are empty, they are replaced by 0 and the number of
    columns, respectively.

    Differences with Unix uniq.
    -

    Todo:
    -
"""

import sys
import re
import os
import getopt

def main(argv):

    try:
        opts, args = getopt.getopt(argv[1:], "hd:f:")
    except getopt.GetoptError, err:
        print >> sys.stderr, str(err)
        print >> sys.stderr, __doc__
        return 2

    # catch conflicting options

    # defaults
    sep = r"\s+"
    sep_out = "\t"
    list = ""
    complement = False
    for o, a in opts:
        if o == "-h":
            print __doc__
            return 0
        elif o == "-d":
            sep = a
            sep_out = a
        elif o == "-f":
            list = a
        else:
            print >> sys.stderr, "%s: Unrecognized option specified" % (argv[0],)
            return 2

    if len(args) > 1:
        print >> sys.stderr, "%s: Too many arguments: specify at most one file \
to uniq" % (argv[0],)
        print >> sys.stderr, __doc__
        return 2
    if len(args) == 1:
        f_in = open(args[0])
    else:
        f_in = sys.stdin


    # Peek ahead how many columns we are talking about.
    l_prev = f_in.readline()
    fields_prev = re.split(sep, l_prev.rstrip('\n'))
    ncols = len(fields_prev)

    if not list:
        idxs = range(ncols)
    else:
        idxs = []
        ranges = list.split(",")
        for r in ranges:
            endpoints = r.split("-")
            if len(endpoints) > 2 or not len(endpoints):
                raise ValueError("%s: Unparseable range in LIST" % (argv[0],))
            if len(endpoints) == 1:
                start = int(endpoints[0])
                end = start
            else:
                if endpoints[0] == "":
                    start = 1
                else:
                    start = int(endpoints[0])
                if endpoints[1] == "":
                    end = ncols
                else:
                    if int(endpoints[1]) > ncols:
                        raise ValueError("%s: Not enough columns in file" % (argv[0],))
                    end = int(endpoints[-1])
            if start > end:
                start -= 1
                end -= 2
                step = -1
            else:
                step = 1
                start -= 1

            idxs = idxs + range(start, end, step)


    for l in f_in:
        fields = re.split(sep, l.rstrip('\n'))
        all_the_same = True
        for i in idxs:
            if fields_prev[i] != fields[i]:
                all_the_same = False
                break
        if not all_the_same:
            print >> sys.stdout, l_prev.rstrip('\n')
            l_prev = l
            fields_prev = fields
    print >> sys.stdout, l_prev.rstrip('\n')

    return 0

if __name__ == "__main__":
    sys.exit(main(sys.argv))
