"""
cut.py

GPL3, Nov 2012
Richard Berendsen

cut.py file

Options:

    -h Show this help
    -d Delimiter (separator) of fields
    -b LIST
       Select only these bytes (Not implemented)
    -c LIST
       Select only these characters (Not implemented)
    -f LIST
       Select only these fields.
    -n Complement (negation).

    LIST is a comma separated list of ranges. A range is either a single
    integer, or a start and endpoint (both inclusive) separated by a hyphen.
    If start- or endpoint are empty, they are replaced by 0 and the number of
    columns, respectively.

    Differences with Unix cut.

    - Unix cut does not support swapping columns, this program does
    - Unix cut does not support printing columns several times, this program
      does.
    - Unix cut allows several files as argument, which are pasted row-wise much
      like cat. This program allows only one file argument. Use cat if you want
      it to work on multiple files.
    - We accept backwards ranges: fields in this range will be printed in
      reverse order.

    Todo:
    - support multiple files, which are then understood as a single file,
      concatenated horizontally. In this way, we can cheaply merge files.
      Files must be of the same length to get dependable results.
"""

import sys
import re
import os
import getopt
import collections
import numpy as np
import blist
import itertools

def main(argv):

    try:
        opts, args = getopt.getopt(argv[1:], "hd:f:n")
    except getopt.GetoptError, err:
        print >> sys.stderr, str(err)
        print >> sys.stderr, __doc__
        return 2

    # catch conflicting options

    # defaults
    sep = r"\s+"
    sep_out = "\t"
    list = ""
    complement = False
    for o, a in opts:
        if o == "-h":
            print __doc__
            return 0
        elif o == "-s":
            sep = a
            sep_out = a
        elif o == "-f":
            list = a
        elif o == "-n":
            complement = True
        else:
            print >> sys.stderr, "%s: Unrecognized option specified" % (argv[0],)
            return 2

    if len(args) > 1:
        print >> sys.stderr, "%s: Too many arguments: specify at most one file to cut from" % (argv[0],)
        print >> sys.stderr, __doc__
        return 2
    if len(args) == 1:
        f_in = open(args[0])
    else:
        f_in = sys.stdin

    if not list:
        print >> sys.stderr, "%s: Please specify which fields you would like to print" % (argv[0],)
        return 2

    # Peek ahead how many columns we are talking about.
    ncols = len(re.split(sep, f_in.readline().rstrip('\n')))
    f_in.seek(0)

    idxs = []
    ranges = list.split(",")
    for r in ranges:
        endpoints = r.split("-")
        if len(endpoints) > 2 or not len(endpoints):
            raise ValueError("%s: Unparseable range in LIST" % (argv[0],))
        if len(endpoints) == 1:
            start = int(endpoints[0])
            end = start
        else:
            if endpoints[0] == "":
                start = 1
            else:
                start = int(endpoints[0])
            if endpoints[1] == "":
                end = ncols
            else:
                if int(endpoints[1]) > ncols:
                    raise ValueError("%s: Not enough columns in file" % (argv[0],))
                end = int(endpoints[-1])
        if start > end:
            start -= 1
            end -= 2
            step = -1
        else:
            step = 1
            start -= 1

        idxs = idxs + range(start, end, step)
    if complement:
        # negate:
        # any field that is mentioned at least once is not returned.
        # all other fields are returned, in the order in which they
        # appear in f_in
        idxs = sorted(set(range(ncols)) - set(idxs))

    for l in f_in:
        fields = re.split(sep, l.rstrip('\n'))
        print sep_out.join([fields[i] for i in idxs])

    return 0

if __name__ == "__main__":
    sys.exit(main(sys.argv))
