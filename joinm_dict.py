"""
joinm_dict.py, pronounced "Join'em Dict".

GPL3, Dec 2012
Richard Berendsen

joinm.py file file

Options:

    -h Show this help
    -s Separator of fields
    -f j
       Join on field j for corresponding file:
       The first occurrence of this option corresponds to the first file,
       the second occurrence to the second file, and so on.
       The last occurrence corresponds to the remaining files.
       Now j can be an integer, or it can be a list of integers, separated
       with a colon, e.g. j 1:2 means that the join will be performed if both
       field 1 and field 2 are matched.
    -o Operator. Choice of: ij|foj|loj.
        - ij: Only output entries that are present in all files (Inner join)
        - foj: Output all entries in any file, even if they are not present in
          one of the remaining files. (Full outer join) (Not implemented)
        - loj: Output all entries in the first file, even if they are not
          present in one of the remaining files. (Left outer join) (Not implemented)
    -p Padding value: what to write in columns corresponding to files in which
       the entry was not present.
    -e hEader present. In all (sorted) input files, the first row is a header.
       This will print the corresponding headers also in the output files
       (Not implemented)


Output: An inner join of all files. All columns of all
files are printed in the output, but the fields on which the join is performed
appear only once, in the beginning of the output.

joinm.py Uses a mimimal amount of memory, but it appears to be rather slow.
joinm_dict.py Will load the first file in memory, in a dictionary. Then, it will
walk through the second file and search in the dictionary (avg: O(1)). Unlike
joinm.py, joinm_dict.py supports joining only two files. But like joinm.py, multiple
fields can serve as key.

"""

import sys
import re
import os
import getopt
import collections

def main(argv):

    try:
        opts, args = getopt.getopt(argv[1:], "hf:s:o:lp:")
    except getopt.GetoptError, err:
        print >> sys.stderr, str(err)
        print >> sys.stderr, __doc__
        return 2


    if len(args) != 2:
        print >> sys.stderr, "%s: need two files to join" % (argv[0],)
        print >> sys.stderr, __doc__
        return 2
    files = args


    # defaults
    op = "ij" # other values: loj, foj
    sep = r"\s+"
    sep_out = "\t"
    pad = "NA"
    join_idxs = []
    for o, a in opts:
        if o == "-h":
            print __doc__
            return 0
        elif o == "-s":
            sep = a
            sep_out = a
        elif o == "-f":
            join_idxs.append(a)
        elif o == "-o":
            if a in ["ij", "foj", "loj"]:
                op = a
            else:
                print >> sys.stderr, "%s: Unrecognized value for -o argument"
                return 2
        elif o == "-p": # only makes sense with -l or -o
            pad = a
        else:
            print >> sys.stderr, "%s: Unrecognized option specified" % (argv[0],)
            return 2


    if op in ["foj", "loj"]:
        raise NotImplementedError

    if join_idxs:
        join_idxs = [[int(y) - 1 for y in x.split(':')] for x in join_idxs]
        if len(join_idxs) > len(files):
            print >> sys.stderr, "%s: Error:  more joining join_idxs than files"
            return 2
        if len(set([len(x) for x in join_idxs])) > 1:
            print >> sys.stderr, "%s: Error: need same number of joining fields in all files"
            return 2
    else:
        join_idxs = [[0]]
    # pad join_idxs to the number of files with the last specified value
    join_idxs = join_idxs + [join_idxs[-1]] * (len(files) - len(join_idxs))

    # loop over files.
    dict_file = open(files[0])
    walk_file = open(files[1])

    # peek ahead in each file, the first line, how many columns does each file have?
    n_fields_dict_file = len(re.split(sep, dict_file.readline().rstrip('\n')))
    n_fields_walk_file = len(re.split(sep, walk_file.readline().rstrip('\n')))
    dict_file.seek(0)
    walk_file.seek(0)

    # check another condition: each file must have at least as many colums as
    # there are joining fields
    if n_fields_dict_file < len(join_idxs[0]) \
            or \
            n_fields_walk_file < len(join_idxs[1]):
        print >> sys.stderr, "%s: Error: not enough columns in some files"
        return 3
    # Of course the join_idxs themselves must be present in the file also, but
    # we cannot keep checking everything.

    d = collections.defaultdict(lambda: [])

    # Fill dict with first file
    for l in dict_file:
        fields = re.split(sep, l.rstrip('\n'))
        if len(fields) != n_fields_dict_file:
            raise ValueError("wrong number of columns")

        data_fields = tuple([f for (i, f) in enumerate(fields) if i not in join_idxs[0]])
        join_fields = tuple([fields[i] for i in join_idxs[0]])

        d[join_fields].append(data_fields)


    # Walk through second file.
    # If entry is in dict, print out all data fields.
    for l in walk_file:
        fields = re.split(sep, l.rstrip('\n'))
        if len(fields) != n_fields_walk_file:
            raise ValueError("wrong number of columns")

        data_fields = tuple([f for (i, f) in enumerate(fields) if i not in join_idxs[1]])
        join_fields = tuple([fields[i] for i in join_idxs[1]])

        try:
            first_data_records = d[join_fields]
        except:
            continue
        else:
            for rec in first_data_records:
                print sep_out.join(join_fields + rec + data_fields)



    return 0

if __name__ == "__main__":
    sys.exit(main(sys.argv))
