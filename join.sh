#!/bin/bash

# This file is part of git@bitbucket.org:RichardBerendsen/rb-unix-util.git,
# and it resides in the same directory as bash_util.sh, which is intended
# to be a useful file to source in any Bash script. So we'll source it here.
script_dir="$( dirname "$( readlink -f "$0" )" )"
source "$script_dir/bash_util.sh"

function usage() {
cat >&2 <<EOF
join.sh
    file1
    field1
    file2
    field2

Options:
    -b buffer_size (used in sort to speed things up)
    -t temporary_directory (used in sort, and also used to store intermediate
            sorted files)

Explanation:
join.sh performs a join between two files, using unix join. But first, it
sorts the two files lexcigraphically on their respective join fields, always
a pain to do manually. Its output is sorted lexicographically on the join field as
well, as a consequence. In addition, to speed up the sort, this script accepts
parameters such as buffer_size and temporary_directory for unix sort.

Note:
join.sh will execute the sorting of the two files in sequence, not in parallel.
The idea here is that if you want you can call many instances of join.sh in
parallel, without worrying about join.sh itself spawning multiple processes.
EOF
}

# default values:
temporary_directory='/tmp'
buffer_size='10M'
out='' # stdout

# get options
while getopts "b:t:o:" opt; do
  case $opt in
    b)
      buffer_size="$OPTARG"
      ;;
    t)
      temporary_directory="$OPTARG"
      ;;
    o)
      out="$OPTARG"
      ;;
    \?)
      echo "Invalid option: -$OPTARG" >&2
      ;;
  esac
done
shift $(($OPTIND - 1))

if [ "$#" -ne "4" ]
then
    usage
    exit 2
fi

file1="$1"
field1="$2"
file2="$3"
field2="$4"

# make temporary directory in temporary_directory for output sort
sort_out="$( mktemp -d "$temporary_directory/sort_out.XXXXXXXXXX" )"

# redirect stdout to file if requested
if [ "$out" != '' ]
then
    does_not_exist_or_die "$out"
    mkdir --parents "$( dirname "$out" )"
    exec > "$out"
fi

# sort files; send error messages over stderr
for i in '1' '2'
do
    field="field$i"
    file="file$i"
    LC_ALL=C sort --field-separator='	' --buffer-size="$buffer_size" \
            --temporary-directory="$temporary_directory" \
            --key="${!field},${!field}" \
            "${!file}" \
            > "$sort_out/$i"
done

# join files, sending output over stdout as usual (and stderr over stderr).
LC_ALL=C join -t'	' -1 "$field1" -2 "$field2" "$sort_out/1" "$sort_out/2"

# clean up after yourself
rm -rf "$sort_out"
