Some scripts I find useful to have in my shell.

bash_util.sh
    Import this one in any bash script for better error reporting and some
    functions that could come in handy.

cut.py
    Early version of a cut program that can do some stuff the usual `cut` can't

joinm_dict.py
    Do a join using a Python dictionary

joinm.py
    A program much like `join` but it has a bit different capabalities.
