"""
joinm.py, pronounced "Join'em".

GPL3, Nov 2012
Richard Berendsen

joinm.py file file [file ...]

Options:

    -h Show this help
    -s Separator of fields
    -f j
       Join on field j for corresponding file:
       The first occurrence of this option corresponds to the first file,
       the second occurrence to the second file, and so on.
       The last occurrence corresponds to the remaining files.
       Now j can be an integer, or it can be a list of integers, separated
       with a colon, e.g. j 1:2 means that the join will be performed if both
       field 1 and field 2 are matched.
    -o Operator. Choice of: ij|foj|loj.
        - ij: Only output entries that are present in all files (Inner join)
        - foj: Output all entries in any file, even if they are not present in
          one of the remaining files. (Full outer join)
        - loj: Output all entries in the first file, even if they are not
          present in one of the remaining files. (Left outer join)
    -p Padding value: what to write in columns corresponding to files in which
       the entry was not present.
    -t j
       For each join field j specified with the -f option, specify the type
       (str|int|float). Remaining unspecified join fields will be assumed to
       have type 'str'.
    -e hEader present. In all (sorted) input files, the first row is a header.
       This will print the corresponding headers also in the output files
       (Not implemented)

Assumptions:
    - All files are sorted by the fields on which they are joined.

Output: If -a is specified, a left outer join of the first file with the
others.  If -a is not specified, an inner join of all files. All columns of all
files are printed in the output, but the fields on which the join is performed
appear only once, in the beginning of the output.

NB: If you'd like joinm.py to work as expected, make sure your files are sorted
in the same way as joinm.py performs comparisons on ids: We sort based on ASCII
currently. If you join on multiple fields, we sort based the first mentioned
field first, then sort on the second field, and so on: Make sure your file is
sorted in the same way.

Motivation for this program: Unix join does not support joining on multiple
fields, or joining more than two files. This program is a straightforward
generalization of join providing these functionalities. The advantage of
the join program and this program over database joins is that it is possible
to work with huge files: memory consumption is kept at a bare minimum, values
for only one joining-tuple are kept in memory at the same time. The advantages
over reading everything into a huge dictionary are: (i) join supports duplicate
ids in the files, (ii) not everything needs to be stored in memory, (iii) no
costly lookups performed into the huge dictionary during loading.

Dependencies: blist
"""

import sys
import re
import os
import getopt
import collections
import numpy as np
import blist
import itertools

"""
Write out all data associated with this join_id
"""
def serialize((join_fields, (join_fields_str, data)), sep_out, op, pad, n_fields_files, join_idxs):

    if op == "foj" or op =="loj":
            # files that did not have an id will contribute pad values

        # if op == "loj", do not add a row with pad values to the first file if
        # it is empty
        if op == "foj":
            start = 0
        else:
            start = 1

        # put one row with pad values in files that had no row for this id.
        for i, f in enumerate(data[start:], start):
            if not f:
                f.append(tuple([pad] * (n_fields_files[i] - len(join_idxs[i]))))

    # print out cartesian product of rows.
    for i in itertools.product(*data):
        print sep_out.join(itertools.chain(*((join_fields_str,) + i)))


def main(argv):

    try:
        opts, args = getopt.getopt(argv[1:], "hf:s:o:lp:t:")
    except getopt.GetoptError, err:
        print >> sys.stderr, str(err)
        print >> sys.stderr, __doc__
        return 2


    if len(args) < 2:
        print >> sys.stderr, "%s: need at least two files to join" % (argv[0],)
        print >> sys.stderr, __doc__
        return 2
    files = args


    # defaults
    op = "ij" # other values: loj, foj
    sep = r"\s+"
    sep_out = "\t"
    pad = "NA"
    join_idxs = []
    join_types = []
    for o, a in opts:
        if o == "-h":
            print __doc__
            return 0
        elif o == "-s":
            sep = a
            sep_out = a
        elif o == "-f":
            join_idxs.append(a)
        elif o == "-t":
            join_types.append(a)
        elif o == "-o":
            if a in ["ij", "foj", "loj"]:
                op = a
            else:
                print >> sys.stderr, "%s: Unrecognized value for -o argument"
                return 2
        elif o == "-p": # only makes sense with -l or -o
            pad = a
        else:
            print >> sys.stderr, "%s: Unrecognized option specified" % (argv[0],)
            return 2


    if join_idxs:
        join_idxs = [[int(y) - 1 for y in x.split(':')] for x in join_idxs]
        if len(join_idxs) > len(files):
            print >> sys.stderr, "%s: Error:  more joining join_idxs than files"
            return 2
        if len(set([len(x) for x in join_idxs])) > 1:
            print >> sys.stderr, "%s: Error: need same number of joining fields in all files"
            return 2
    else:
        join_idxs = [[0]]
    # pad join_idxs to the number of files with the last specified value
    join_idxs = join_idxs + [join_idxs[-1]] * (len(files) - len(join_idxs))

    type2cast = {'str':lambda x: x, 'int':int, 'float':float}
    if join_types:
        join_types = [[type2cast[y] for y in x.split(':')] for x in join_types]
        if len(join_types) > len(files):
            print >> sys.stderr, "%s: Error:  more joining join_types than files"
            return 2
        if len(set([len(x) for x in join_types])) > 1:
            print >> sys.stderr, "%s: Error: need same number of joining types in all files"
            return 2
    else:
        join_types = [[type2cast["str"]]]
    # pad join_types to the number of files with the last specified value
    join_types = join_types + [join_types[-1]] * (len(files) - len(join_types))

    # loop over files. Remember they are all sorted by field_id
    fs = [open(f) for f in files]
    f_idxs = range(len(fs))


    # the datastructure where we keep all values stored so far until we can
    # write them out.  it is a sorted dict, where the first item is the
    # smallest, just like we like.
    # [id]-->[ # list of files/tables
    #           [ # list of rows/records with this id
    #               [ # list of columns in this record
    #       ]]]
    id2data = blist.sorteddict()


    # peek ahead in each file, the first line, how many columns does each file have?
    n_fields_files = [len(re.split(sep, f.readline().rstrip('\n'))) for f in fs]
    for f in fs:
        f.seek(0)

    # check another condition: each file must have at least as many colums as
    # there are joining fields
    for i, j in zip(n_fields_files, join_idxs):
        if i < len(j):
            print >> sys.stderr, "%s: Error: not enough columns in some files"
            return 3
    # Of course the join_idxs themselves must be present in the file also, but
    # we cannot keep checking everything.


    while f_idxs:
            # NB FIXME TODO: the logic breaks here because fs changes in size,
            # and so this messes up the datastructure in various ways.

        lowest_observed_id_this_round = None

        # read one line in each file (one round)
        for i in f_idxs:

            l = fs[i].readline().rstrip('\n')

            # if EOF, close f now, continue, and delete f from fs (after looping)
            if l == "": # EOF
                fs[i].close()
                continue


            # procces l, storing observed values
            fields = re.split(sep, l)
            if len(fields) != n_fields_files[i]:
                raise ValueError(\
                    "File %s does not have the same number of colums everywhere" % fs[i].name)
            data_fields = tuple([f for (j, f) in enumerate(fields) if j not in join_idxs[i]])
            join_fields = tuple([f(fields[j]) for f,j in zip(join_types[i], join_idxs[i])])
            join_fields_str = tuple([fields[j] for j in join_idxs[i]])

            # keep track of lowest_id_observed.
            if lowest_observed_id_this_round is None \
                    or join_fields < lowest_observed_id_this_round:
                lowest_observed_id_this_round = join_fields


            # store data
            if join_fields not in id2data:
                id2data[join_fields] = (join_fields_str, [[] for j in range(len(fs))])
            id2data[join_fields][1][i].append(data_fields)


        # remove indexes of closed files before looping again
        f_idxs = [i for i in f_idxs if not fs[i].closed]


        # check what we can write to stdout of the stored data.
        while id2data.keys() and id2data.keys()[0] < lowest_observed_id_this_round:
            serialize(id2data.popitem(), sep_out, op, pad, n_fields_files, join_idxs)


    # write out remaining items in id2data
    while id2data.keys():
        serialize(id2data.popitem(), sep_out, op, pad, n_fields_files, join_idxs)


    return 0

if __name__ == "__main__":
    sys.exit(main(sys.argv))
