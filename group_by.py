"""
group_by.py

GPL3, Nov 2012
Richard Berendsen

group_by.py file

Options:

    -h Show this help
    -d Delimiter (separator) of fields
    -g SET
       Group by these fields. Specify like in the unix cut program; it will be
       treated as a set, though.
    -m SET
       Manipulate or Modify these fields. Specifiy like with -g. 
    -f FUNCTION_LIST
       Aggregate the columns in -m using functions from FUNCTION_LIST.

       A  function can appear multiple times here, if you want that for some
       reason. Also, multiple FUNCTION_LIST's can appear. If so, consecutive
       FUNCTION_LIST's will be used on consecutive fields specified in with -m,
       until FUNCTION_LIST's have been exhausted, in which case the last
       FUNCTION_LIST will be used on the remaining fields specified with -m.

       NB: the order of fields specified with -m is the sorted order, not the
       order specified in in the argument to the -m option. So, if you specified
       -m 2,1 the order is 1,2. Therefore, the first FUNCTION_LIST will apply to
       column 1, not 2.

Grammar:
    FUNCTION_LIST: FUNCTION_LIST FUNCTION
    
    FUNCTION: ( n | sum | mean | std | min | max | str_hist )

Explanation:
    group_by.py will group all rows together that are the same up to the fields
    in -g and -m.
    Then it will use on each field in -m all the FUNCTION's in FUNCTION_LIST. 
    
    In the output the columns in -g will disappear, and each column in -m will
    be replaced by the output of as many functions as there are in
    FUNCTION_LIST, each output in its own field. The remaining fields will appear
    where they appeared before, if there are no remaining fields only one row will
    be output.

    It will do running implementations of each, not keeping all data in memory, but only
    the unique rows of the remaining fields (not used to group by or manipulated)
"""

import sys
import re
import os
import getopt
import collections
import numpy as np
import itertools

def parse_set(set_str, ncols):
    """
    ACCEPTS unix cut like arguments, e.g. 1-2,5-7,4, meaning fields counted
    starting from one.
    RETURNS a set of indices to be included, counting the fields starting from
    0
    """
    idxs = []
    if set_str == '':
        return idxs
    ranges = set_str.split(",")
    for r in ranges:
        endpoints = r.split("-")
        if len(endpoints) > 2 or not len(endpoints):
            raise ValueError("%s: Unparseable range in SET" % (argv[0],))
        if len(endpoints) == 1:
            start = int(endpoints[0])
            end = start
        else:
            if endpoints[0] == "":
                start = 1
            else:
                start = int(endpoints[0])
            if endpoints[1] == "":
                end = ncols
            else:
                if int(endpoints[1]) > ncols:
                    raise ValueError("%s: Not enough columns in file" % (argv[0],))
                end = int(endpoints[1])
        if start > end:
            start -= 1
            end -= 2
            step = -1
        else:
            step = 1
            start -= 1

        idxs = idxs + range(start, end, step)
    return sorted(set(idxs))

def n(d):
    return ('%d', d['n'])
def sum(d):
    return ('%.8e', d['sum'])
def mean(d):
    return ('%.8e', d['sum'] / d['n'])
def std(d):
    return ('%.8e', np.sqrt(d['ss'] / d['n'] - (d['sum'] / d['n'])**2))
def min(d):
    return ('%.8e', d['min'])
def max(d):
    return ('%.8e', d['max'])
def str_hist(d):
    return ('%s', ', '.join(['%s (%d)' % (x, n) for x, n in sorted(d['str_hist'].iteritems())]))

def parse_function_list(function_set):
    """
    Returns a set of function pointers
    """
    str2fun = collections.OrderedDict([
            ('n', n),
            ('sum', sum),
            ('mean', mean),
            ('std', std),
            ('min', min),
            ('max', max),
            ('str_hist', str_hist)
    ])
    if function_set == '':
        return []
    functions = [str2fun[s.lower()] for s in function_set.split(',')]
    return functions

def get_modify_idxs_with_functions(modify_sets, function_sets, ncols):
    d = {}
    if len(modify_sets) < len(function_sets):
        raise ValueError('More function sets than modify idx sets specified')
    # Pad function_sets if necessary
    if len(modify_sets) > len(function_sets):
        last_function_set = function_sets[-1]
        for i in range(len(modify_sets) - len(function_sets)):
            function_sets.append(last_function_set)
    # Now modify_sets and function_sets should have equal length.

    # We can fill d (modify_idx 2 function_list)
    for ms, fs in zip(modify_sets, function_sets):
        modify_idxs = parse_set(ms, ncols)
        functions = parse_function_list(fs)
        for i in modify_idxs:
            for j in functions:
                if i in d:
                    d[i].append(j)
                else:
                    d[i] = [j]
    return d

def calc(d, functions):
    return [f(d) for f in functions]

def main(argv):

    try:
        opts, args = getopt.getopt(argv[1:], "hd:g:m:f:")
    except getopt.GetoptError, err:
        print >> sys.stderr, str(err)
        print >> sys.stderr, __doc__
        return 2

    # catch conflicting options

    # defaults
    sep = r"\s+"
    sep_out = "\t"
    group_by_set = ""
    modify_sets = []
    function_sets = []
    for o, a in opts:
        if o == "-h":
            print __doc__
            return 0
        elif o == "-d":
            sep = a
            sep_out = a
        elif o == "-g":
            group_by_set = a
        elif o == "-m":
            modify_sets.append(a)
        elif o == "-f":
            function_sets.append(a)
        else:
            print >> sys.stderr, "%s: Unrecognized option specified" % (argv[0],)
            return 2

    if len(args) > 1:
        print >> sys.stderr, "%s: Too many arguments: specify at most one file" % (argv[0],)
        print >> sys.stderr, __doc__
        return 2
    if len(args) == 1:
        f_in = open(args[0])
    else:
        f_in = sys.stdin

    # Peek ahead how many columns we are talking about.
    ncols = len(re.split(sep, f_in.readline().rstrip('\n')))
    f_in.seek(0)

    # Parse the indexes
    group_by_idxs = parse_set(group_by_set, ncols)

    # Get a mapping from modify_idxs to functions
    modify_idxs2function_lists = get_modify_idxs_with_functions(modify_sets, function_sets, ncols)

    all_modify_idxs = sorted(modify_idxs2function_lists.keys())

    if not set(group_by_idxs).isdisjoint(set(all_modify_idxs)):
        print >> sys.stderr, "%s: Group by and modify fields should be disjoint" % (argv[0],)
        return 3

    # 'd' will hold the data
    d = {}

    # process the data in d, discarding group by colunms
    for line_no, l in enumerate(f_in, start=1):
        fields = re.split(sep, l.rstrip('\n'))
        if len(fields) != ncols:
            print >> sys.stderr, "Line %d has incorrect number of fields:" % (line_no,)
            print >> sys.stderr, "%s" % (l,)
            sys.exit(3)
        k = tuple([fields[i] for i in group_by_idxs])
        if k not in d:
            d[k] = [{ 'n' : 0, 'sum' : 0.0,
                    'ss' : 0.0, 'min' : float('inf'), 'max' : float('-inf'),
                    'str_hist' : {}} for i in all_modify_idxs]
        for j, s  in enumerate([fields[i] for i in all_modify_idxs]):
            x = float(s)
            d[k][j]['n'] += 1
            d[k][j]['sum'] += x
            d[k][j]['ss'] += x**2
            if x < d[k][j]['min']:
                d[k][j]['min'] = x
            if x > d[k][j]['max']:
                d[k][j]['max'] = x
            if s in d[k][j]['str_hist']:
                d[k][j]['str_hist'][s] += 1
            else:
                d[k][j]['str_hist'][s] = 1

    # and now do the final calculations and output. 
    for k, v in d.iteritems():
        out_fields = []
        k = list(k)
        v = list(v)
        # walk through original columns and see what to do
        for i in range(ncols):
            if i in group_by_idxs:
                out_fields.append(k.pop(0))
            elif i in all_modify_idxs:
                out_fields += [format_string % (x,) for format_string, x in calc(v.pop(0), modify_idxs2function_lists[i])]
            # ignore remaining indexes 
        print sep_out.join(out_fields)

    return 0

if __name__ == "__main__":
    sys.exit(main(sys.argv))
